FROM ubuntu:14.04

RUN apt-get update && apt-get install -y curl nginx supervisor && apt-get -y autoclean

ADD nginx/base.conf /etc/nginx/sites-available/
ADD supervisord/program.conf /etc/supervisor/conf.d/program.conf

RUN curl -L https://download.elasticsearch.org/kibana/kibana/kibana-3.1.1.tar.gz | tar xz -C /opt/ && mv /opt/kibana-3.1.1 /opt/kibana && curl -L http://grafanarel.s3.amazonaws.com/grafana-1.8.1.tar.gz | tar zx -C /opt/ && mv /opt/grafana-1.8.1 /opt/grafana && ln -s /etc/nginx/sites-available/base.conf /etc/nginx/sites-enabled/base.conf && rm /etc/nginx/sites-available/default && echo 'daemon off;' >> /etc/nginx/nginx.conf

ADD grafana/config.js /opt/grafana/
ADD nginx/index.html /opt/index.htm

CMD /usr/bin/supervisord
